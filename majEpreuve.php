<?php
require_once('assets/php/main.php');
$db = get_db();
?>
<form id="maj-form" onsubmit="majEpreuve(); return false;" method="post">
  <div id="maj-wrapper">
    <label for="maj-bts">Sélectionner BTS</label>
    <select id="maj-bts" onchange="majEpreuve2();" name="maj-bts">
      <?php
      echo '<option value="0" selected>Selectionner BTS</option>';
      $sql = "SELECT DISTINCT comporter.idBts, bts.codeBts FROM bts, comporter WHERE bts.idBts = comporter.idBts ORDER BY bts.codeBts ASC";
      $res = $db->query($sql);
      $empty = true;
      while ($row = $res->fetch_row()) {
        $empty = false;
        echo '<option value="'.$row[0].'">'.$row[1].'</option>';
      }
      $res->close();
      ?>
    </select>
    <label for="maj-epreuve">Sélectionner épreuve</label>
    <select id="maj-epreuve" onchange="majLists(); updateInputs();" name="maj-epreuve">
    </select>
    <label for="maj-date">Date épreuve</label>
    <input id="maj-date" type="text" name="maj-date"/>
    <label for="maj-time">Heure épreuve</label>
    <input id="maj-time" type="text" name="maj-time"/>
  </div>
  <input type="submit" id="maj-sub" value="Valider"/>
  <div id="maj-sorts"></div>
</form>
<?php
  if($empty) {
    ?>
      <script type="text/javascript">
        $('#hpform').load('majEpreuveNeed.php');
      </script>
    <?php
  }
?>

<script>

$('#maj-time').timepicker({ 'timeFormat': 'H:i:s' });
$(function() {
  $( "#maj-date" ).datepicker({
    changeMonth: true,
    changeYear: true,
    altField: "#be-time",
    closeText: 'Fermer',
    prevText: 'Précédent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekHeader: 'Sem.',
    dateFormat: 'yy-mm-dd'
  });
});

function majLists() {
  console.log("majLists()");
  var data = {};
  data['bts'] = $('#maj-bts').val();
  data['epreuve'] = $('#maj-epreuve').val();
  data['heure'] = $('#maj-time').val();
  data['date'] = $('#maj-date').val();
  $('#maj-sorts').load('majLists.php', data);
}
function majEpreuve2(){
  console.log("majEpreuve2()");
  var data = {};
  data['bts'] = $('#maj-bts').val();
  $('#maj-epreuve').load('majEpreuve2.php', data, function() {
    console.log('majEpreuve2 callback');
    updateInputs();
  });
}

function majEpreuve(){
  var show_message = $('#msg_all').stop(true, false).fadeIn(300).delay(2000).fadeOut(300);
  var data = {};
  data['bts'] = $('#maj-bts').val();
  data['epreuve'] = $('#maj-epreuve').val();
  data['heure'] = $('#maj-time').val();
  data['date'] = $('#maj-date').val();
  $.ajax({
    url: 'majAjax.php',
    type: 'POST',
    data: data
  })
  .always(function(e) {
    console.log(e);
  })
  .done(function(e) {
    console.log(e);
    show_message;
    $("#msg_all").html("Formulaire envoyé");
    $("#be-time").val('');
    $('#be-bts, #be-epreuve').prop('selectedIndex',0);
    majLists();
  })
  .fail(function(e) {
    $("#msg_all").html("Erreur");
    console.log(e);
  });

}

function updateInputs() {
  console.log('updateInputs');
  var data = {};
  data['bts'] = $('#maj-bts').val()
  data['epreuve'] = $('#maj-epreuve').val()
  console.log(data);
  $.ajax({
    url: 'majUpdateInputs.php',
    type: 'POST',
    data: data
  })
  .always(function(e) {
    if(e !== "") {
      var fdata = JSON.parse(e);
      $('#maj-time').val(fdata.heure);
      $('#maj-date').val(fdata.date);
    } else {
      $('#maj-time').val("");
      $('#maj-date').val("");
    }
    majLists();
  });

}

majEpreuve2()

</script>
