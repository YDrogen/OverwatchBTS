<?php
require_once('assets/php/main.php');
$db = get_db();
$sql = "SELECT idBts,codeBts FROM bts ORDER BY codeBts ASC";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>BTS-Epreuve :</title>
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
  <link  rel="shortcut icon" href="assets/img/surveillance-eye-symbol.svg">
  <link rel="stylesheet" href="assets/css/main.css"/>
</head>
<body id="be-body">
  <div id="be-content">
    <form id="be-form" onsubmit="btsEpreuve(); return false;" method="post">
      <div id="be-wrapper">
        <label for="be-bts">Selectionner BTS :</label>
        <select id="be-bts" name="be-bts">
          <?php
          $res = $db->query($sql);
          $empty = true;
          while ($row = $res->fetch_row()) {
            $empty = false;
            echo '<option value="'.$row[0].'">'.$row[1].'</option>';
          }
          ?>
        </select>
        <label for="be-epreuve">Selectionner une épreuve</label>
        <select id="be-epreuve" name="be-epreuve">
          <?php
          $sql = "SELECT idEpreuve,libelleEpreuve FROM epreuve ORDER BY libelleEpreuve ASC";
          $res = $db->query($sql);
          if(!$empty) {
            $empty = true;
          }
          while ($row = $res->fetch_row()) {
            $empty = false;
            echo '<option value="'.$row[0].'">'.$row[1].'</option>';
          }
          ?>
        </select>
        <label for="be-time">Durée</label>
        <input id="be-time" type="text" name="be-time"/><span>h</span>
        <input id="be-sub" type="submit" value="Affecter"/><br />
        <span id="msg_all"></span>
      </div>
<?php
if($empty) {
  ?>
    <script type="text/javascript">
      $('#hpform').load('btsEpreuveNeed.php');
    </script>
  <?php
}
?>

    </form>
  </div>
</body>
</html>
