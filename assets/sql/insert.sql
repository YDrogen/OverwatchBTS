USE overwatchbts;

INSERT INTO bts (codeBts, libelleBts) VALUES ('sio2', 'Services d\'Informations aux Organisations 2'), ('com2', 'Communication 2'), ('sio1', 'Services d\'Informations aux Organisations 1'), ('com1', 'Communication 1');
-- INSERT INTO epreuve (codeEpreuve, libelleEpreuve, dureeEpreuve) VALUES ('fr', 'Français', 4), ('ang', 'Anglais', 2), ('edm', 'Economie, Droit, Management', 4);
INSERT INTO epreuve (codeEpreuve, libelleEpreuve) VALUES ('fr', 'Français'), ('ang', 'Anglais'), ('edm', 'Economie, Droit, Management');
INSERT INTO prof (nom, prenom, nbConvoc) VALUES ('Dupont', 'Jean', 0), ('Zuker', 'Mark', 0), ('Grills', 'Ben', 0);
INSERT INTO salle (numSalle, capacite) VALUES ('A31', 20), ('A34', 25), ('D21', 30);
