<h1 class="cgh1" id="cgform">Formulaire</h1>
<form id="cg-form">
  <center>
    <label for="nom">Nom</label><br>
    <input class="a_inpt" id="nom" type="text" name="nom" pattern="[A-Za-z]*" title="Mettre que des lettres"></br></br>
    <label for="prenom">Prénom</label><br>
    <input class="a_inpt" id="prenom" type="text" name="prenom" pattern="[A-Za-z]*" title="Mettre que des lettres"></br></br>
    <input type="hidden" name="data" value="">
    <button type="submit" value="Submit" disabled="disabled">Valider</button><br>
    <span id="msg_all"></span>
  </center>
</form>
