<h1 class="cgh1" id="cgcolor">Couleurs utilisées</h1>
<section class="color_content">
  <div class="block_color" style="background-color: #333333"></div>
  <div class="def_color">
    <p>$color-black-20-background</p>
    <p>#333333</p>
  </div>
</section>
<section class="color_content">
  <div class="block_color" style="background-color: #5599FF"></div>
  <div class="def_color">
    <p>$color-blue-hover</p>
    <p>#5599FF</p>
  </div>
</section>
<section class="color_content">
  <div class="block_color" style="background-color: #3A74D1"></div>
  <div class="def_color">
    <p>$color-blue-border</p>
    <p>#3A74D1</p>
  </div>
</section>
