<h1 class="cgh1" id="cgfont">Polices d'ecriture</h1>
<div>
  <p>
    Primary font: "Open sans", sans-serif;
  </p>
  <p id="italic-font">
    Primary font italic: "Open sans", sans-serif;
  </p>
  <p id="bold-font">
    Primary font bold: "Open sans", sans-serif;
  </p>
</div>
