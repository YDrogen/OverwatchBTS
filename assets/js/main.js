function ajoutProf(){
  var show_message = $('#msg_all').stop(true, false).fadeIn(300).delay(2000).fadeOut(300);
  $.ajax({
    url: 'ajout.php',
    type: 'POST',
    data: $("#form").serialize()
  })
  .done(function() {
    show_message;
    $("#msg_all").html("Formulaire envoyé");
    $(".a_inpt").val('');
  })
  .fail(function() {
    $("#msg_all").html("Erreur");
  });
}
function btsEpreuve(){
  var show_message = $('#msg_all').stop(true, false).fadeIn(300).delay(2000).fadeOut(300);
  $.ajax({
    url: 'btsepreuveProc.php',
    type: 'POST',
    data: $("#be-form").serialize()
  })
  .done(function() {
    show_message;
    $("#msg_all").html("Formulaire envoyé");
    $("#be-time").val('');
    $('#be-bts, #be-epreuve').prop('selectedIndex',0);
  })
  .fail(function() {
    $("#msg_all").html("Erreur");
  });
}
function loadDelete() {
  var value = document.getElementsByName($("#select").val())[0].value;
  var data = {};
  data["data"] = $("#select").val();
  data["cond"] = value;
  $("#tableau").load("supprimer.php", data);
};
function loadModif() {
  var value = document.getElementsByName($("#select").val())[0].value;
  var data = {};
  data["data"] = $("#select").val();
  data["cond"] = value;
  $("#tableau").load("modifier.php", data);
};
function checkCheckbox() {
  var checkedList = {};
  if($('#deleteForm :checked').length > 0) {
    $('#deleteFormSubmit').disabled = false;
    checkedList = {};
    for (var i = 0; i < $('#deleteForm :checked').length; i++) {
      checkedList[i] = $('#deleteForm :checked').eq(i).val();
    }
    console.log(checkedList);
    return checkedList;
  } else {
    $('#deleteFormSubmit').disabled = true;
    return false;
  }
};
function loadRecherche(){
  var critere = document.getElementById("barreDeRecherche").value;
  var dataR= {};
  dataR["critere"] = critere;
  $("#tableau").load("supprimer.php",dataR);
};
//HP - Colorisation menu quand cliqué
$('.hpli').click(function() {
  $('.hpli').removeClass('coloring');
  $(this).toggleClass('coloring');
});
//pas toucher
$('.cgilbtn').click(function() {
  var leftC = $('#left_content');
  if(leftC.hasClass('active')){
    leftC.removeClass('active');
    $('html, body').css({'overflow':'auto','height':'100%','margin':'0'});
  }else{
    leftC.toggleClass('active');
    $('html, body').delay( 550 ).queue(function(next){
      $(this).css({'overflow':'hidden','height':'100%','margin':'0'});
      next();
    })
  }
});
//2e etape
$('.cgil').click(function() {
  var leftC = $('#left_content');
  leftC.removeClass('active');
  $('html, body').delay( 300 ).queue(function(next){
    $(this).css({'overflow':'auto','height':'100%','margin':'0'});
    next();
  })
});
//3e etape
$('.cgcross').click(function() {
  var leftC = $('#left_content');
  leftC.removeClass('active');
  $('html, body').delay( 300 ).queue(function(next){
    $(this).css({'overflow':'auto','height':'100%','margin':'0'});
    next();
  })
});

$('#help').click(function() {
    alert('Les éléments sont enregistrés automatiquement lorsque glissés dans la bonne colonne.');
})
