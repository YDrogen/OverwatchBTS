<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once('assets/php/main.php');
$db = get_db();

$bts = $_REQUEST['bts'];
$epreuve = $_REQUEST['epreuve'];
$date = $_REQUEST['date'];
$heure = $_REQUEST['heure'];

if(empty($bts) || empty($epreuve) || empty($date) || empty($heure)) {
  exit();
}

$heureDebut = "08:00:00";

if($heure > "13:00:00") {
  $heureDebut = "13:00:00";
}

$sql = "SELECT salle.idSalle, numSalle FROM salle WHERE salle.idSalle NOT IN (SELECT occuper.idSalle FROM occuper, comporter WHERE occuper.idBts = comporter.idBts AND occuper.idEpreuve = comporter.idEpreuve AND comporter.dateEpreuve = '".$date."' AND comporter.heureDebut >= '".$heureDebut."' AND comporter.heureDebut BETWEEN '".$heure."' AND (cast(CONCAT((LEFT('".$heure."',2)+(SELECT DISTINCT comporter.duree FROM occuper, comporter WHERE occuper.idBts = comporter.idBts AND occuper.idEpreuve = comporter.idEpreuve)),(RIGHT('".$heure."',6))) as time) - 1));";
echo '<ul id="notMaj" class="linkedSort">';
$res = $db->query($sql);
while ($row = $res->fetch_row()) {
  echo '<li class="ui-state-default" value="'.$row[0].'">'.$row[1].'</li>';
}
echo '</ul>';
$res->close();

$sql = "SELECT salle.idSalle, numSalle FROM occuper, salle WHERE occuper.idSalle = salle.idSalle AND occuper.idBts ='".$bts."' AND occuper.idEpreuve = '".$epreuve."'; ";
echo '<ul id="isMaj" class="linkedSort">';
$res = $db->query($sql);
while ($row = $res->fetch_row()) {
  echo '<li class="ui-state-highlight" value="'.$row[0].'">'.$row[1].'</li>';
}
echo '</ul>';
// echo $sql;
$res->close();
?>
<script>

$( function() {
  $( "#notMaj, #isMaj" ).sortable({
    connectWith: ".linkedSort",
    revert: true
  }).disableSelection();
} );

$( "#isMaj" ).on( "sortreceive", function( event, ui ) {
  var data = {};
  data['salle'] = ui.item[0].attributes['value'].value;
  data['bts'] = $('#maj-bts').val();
  data['epreuve'] = $('#maj-epreuve').val();
  console.log(data);
  $.ajax({
    url: 'addMaj.php',
    type: 'POST',
    data: data
  })
  .always(function(e) {
    console.log(e);
    majLists();
  });
});

$( "#notMaj" ).on( "sortreceive", function( event, ui ) {
  var data = {};
  data['salle'] = ui.item[0].attributes['value'].value;
  data['bts'] = $('#maj-bts').val();
  data['epreuve'] = $('#maj-epreuve').val();
  console.log(data);
  $.ajax({
    url: 'removeMaj.php',
    type: 'POST',
    data: data
  })
  .always(function(e) {
    console.log(e);
    majLists();
  });
});

</script>
