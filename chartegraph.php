<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Charte Graphique</title>
  <link rel="stylesheet" href="assets/css/main.css"/>
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="icon" href="assets/img/surveillance-eye-symbol.svg">
</head>
<body id="cgbody">
  <div id="all_content">
    <div id="left_content">
      <ul id="cgul">
        <li class="cgcross"><i class="fa fa-times" aria-hidden="true"></i></li>
        <a href="#cgcolor"><li class="cgil">Couleur</li></a><!--
     --><a href="#cgli"><li class="cgil">Liste</li></a><!--
     --><a href="#cgnav"><li class="cgil">Menu</li></a><!--
     --><a href="#cgform"><li class="cgil">Formulaire</li></a><!--
     --><a href="#cgbtn"><li class="cgil">Bouton</li></a><!--
     --><a href="#cgimg"><li class="cgil">Image</li></a><!--
     --><a href="#cglogo"><li class="cgil">Logo</li></a><!--
     --><a href="#cgfont"><li class="cgil">Police</li></a>
      </ul>
    </div>
    <div id="cgheader">
      <span class="cgilbtn"><i class="fa fa-bars" aria-hidden="true"></i></span>
    </div>
    <div id="right_content">
      <div class="backhome" onclick="document.location.href='index.php'"><span class="cg-triangle"></span>Retour page principale</div>
      <div id="rinner_content">
        <?php
        include 'assets/charte/all.php';
        ?>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>
